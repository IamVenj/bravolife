<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReferralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referrals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('referrer_name');
            $table->string('referrer_organization');
            $table->string('referrer_address');
            $table->string('referrer_email');
            $table->string('referrer_contact_tel');
            $table->string('applicant_first_name');
            $table->string('applicant_sur_name');
            $table->date('applicant_date_of_birth');
            $table->integer('applicant_age');
            $table->string('applicant_place_of_birth');
            $table->longText('applicant_disability_need');
            $table->boolean('archived')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referrals');
    }
}