<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\CompanySetting;
use App\CustomAbout;
use App\Help;
use App\Service;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});

$factory->define(CustomAbout::class, function (Faker $faker) {
    return [
        'image_url' => $faker->name,
        'main_slug' => $faker->unique()->safeEmail,
        'our_vision' => $faker->unique()->safeEmail,
        'our_mission' => now(),
    ];
});

$factory->define(Service::class, function (Faker $faker) {
    return [
        'service' => $faker->words
    ];
});

$factory->define(Help::class, function (Faker $faker) {
    return [
        'slug' => $faker->words
    ];
});

$factory->define(CompanySetting::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->name,
        'location' => $faker->name,
        'phone_number' => $faker->name,
        'accommodation' => $faker->words,
        'image_url' => $faker->name,
        'twitter' => $faker->name,
        'facebook' => $faker->name,
        'linked_in' => $faker->name,
        'google_plus' => $faker->name
    ];
});