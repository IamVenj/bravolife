<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        factory(App\User::class)->create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('bravolife_admin'),
        ]);

        DB::table('company_settings')->delete();
        factory(App\CompanySetting::class)->create([
            'name' => 'BravoLife',
            'email' => 'bravolife@gmail.com',
            'location' => '5 Wolsey Way, Chessington, KT9 1XQ',
            'phone_number' => '+02080149563',
            'accommodation' => 'Our staffs are well experienced and have the cultural capital to support asylum-seeking children and young people and are sensitive to their needs. Our staffs are trained to support child victims of modern slavery and are aware of risks of them going missing. They are also trained detect any risk to the child from those who wish to exploit them. They also know what practical steps they should take in the event that a child goes missing, or if they suspect that someone is trying to lure the child away from their care placement. They are also trained to be fully aware of the child’s or the young person’s past experiences and any psychological issues they face, which may not be immediately apparent, as well as understanding cultural issues, which may put them at greater risk of going missing.
            When an asylum-seeking child or young person is placed in our accommodation by a local authority, we assign each young person their own key worker who can build rapport, work with them to support their needs and liaise with the local authority social workers, personal advisors or housing to ensure continuity of support. We understand the importance of need of these young people to understand who their key worker is and have easy access to them.
            We have robust systems of recruitment, induction, supervision and development of our support staff.
            •	Our support staff will undergo enhanced background checks via DBS (Disclosure and Barring Check Service) check.
            •	Our support staff will undergo specific awareness training in cultural awareness, differing religious needs and legal support required when working with unaccompanied asylum-seeking children and young people.
            •	Our support staff will participate in an induction programme which includes awareness raising about all aspects of the service provision and training on safeguarding.
            •	Our support staff will get ongoing training provided, to enhance staff knowledge and skills based on an analysis of individual learning and development needs.
            •	Our support staff will have clear processes for receiving new arrivals to ensure all immediate medical, physical, legal and safety needs are met.
            •	We will have a 24-hour period where staff on the premises can support them.
            •	We will have a written protocol to be followed in the event of an allegation being made about a support worker. The young person and support worker will know how they will be supported in the event of an allegation being made.
            •	We will have clear processes to ensure the property is safe and secure for the children and young people.
            ',
            'image_url' => 'http://res.cloudinary.com/nfco/image/upload/v1577256373/arq5flg3s5iloe0ennml.png',
            'twitter' => '',
            'facebook' => '',
            'linked_in' => '',
            'google_plus' => ''
        ]);

        DB::table('custom_abouts')->delete();
        factory(App\CustomAbout::class)->create([
            'image_url' => 'http://res.cloudinary.com/nfco/image/upload/v1577256373/arq5flg3s5iloe0ennml.png',
            'main_slug' => '
                BravoLife endeavors to help asylum-seeking children and young people, age 16 years to 25 years old, deal with any worries they may have about in care and leaving care by providing them with information on practical help they will need with accommodation, education, training and employment, money matters, health and other like skills needed as they prepare for independence. BravoLife will also point them in the right direction for advice and support as well as make sure they are listened to and have a say in what happens. It is important to us that these asylum-seeking children and young people get the help and support necessary to make sure they know what happens next, to know who will help them and exactly what they are entitled to.
                Our service will take particular account of the need to protect asylum-seeking children and young people from any risk of being exploited, and from a heightened risk of them going missing. Our semi-independent accommodation can be accessed directly at any time of the day or night where there are sufficient supervision and monitoring by on-site staff to keep asylum-seeking children and young people safe. BravoLife will partner with Local Authorities’ Children Services to support asylum-seeker.
            ',
            'our_vision' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiu smod tempor incidunt enim ad minim veniam quis nostrud exerc tation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiu smod tempor incidunt enim ad minim veniam quis nostrud exerc tation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat.',
            'our_mission' => 'We aim to provide semi-independent accommodation and support asylum-seeking children and young people to help navigate their way through the big transitions in their life as they try to settle in the UK and move into adulthood. ',
        ]);

        DB::table('services')->delete();
        factory(App\Service::class)->create([
            'service' => 'Our staffs are well experienced and have the cultural capital to support asylum-seeking children and young people and are sensitive to their needs. Our staffs are trained to support child victims of modern slavery and are aware of risks of them going missing. They are also trained detect any risk to the child from those who wish to exploit them. They also know what practical steps they should take in the event that a child goes missing, or if they suspect that someone is trying to lure the child away from their care placement. They are also trained to be fully aware of the child’s or the young person’s past experiences and any psychological issues they face, which may not be immediately apparent, as well as understanding cultural issues, which may put them at greater risk of going missing.
            When an asylum-seeking child or young person is placed in our accommodation by a local authority, we assign each young person their own key worker who can build rapport, work with them to support their needs and liaise with the local authority social workers, personal advisors or housing to ensure continuity of support. We understand the importance of need of these young people to understand who their key worker is and have easy access to them.
            We have robust systems of recruitment, induction, supervision and development of our support staff.
            •	Our support staff will undergo enhanced background checks via DBS (Disclosure and Barring Check Service) check.
            •	Our support staff will undergo specific awareness training in cultural awareness, differing religious needs and legal support required when working with unaccompanied asylum-seeking children and young people.
            •	Our support staff will participate in an induction programme which includes awareness raising about all aspects of the service provision and training on safeguarding.
            •	Our support staff will get ongoing training provided, to enhance staff knowledge and skills based on an analysis of individual learning and development needs.
            •	Our support staff will have clear processes for receiving new arrivals to ensure all immediate medical, physical, legal and safety needs are met.
            •	We will have a 24-hour period where staff on the premises can support them.
            •	We will have a written protocol to be followed in the event of an allegation being made about a support worker. The young person and support worker will know how they will be supported in the event of an allegation being made.
            •	We will have clear processes to ensure the property is safe and secure for the children and young people.
            '
        ]);

        DB::table('helps')->delete();
        factory(App\Help::class)->create([
            'slug' => '<b>Our Team</b><br/>
            BravoLife Team will help support asylum-seeking children and young people as they learn to live more independently.
            Our staff will:
            •	Work with the allocated personal advisors/social workers from the Local Authority
            •	Work personal advisors/social workers to develop (co-production) an individual pathway plan
            •	Offer asylum-seeking children and young people with financial advice
            •	Support asylum-seeking children and young people with education, training and seek employment (for with rights to work)
            •	Help asylum-seeking children and young people stay fit and healthy
            •	Ensure we listen to asylum-seeking children and young people and know what to do if they are not happy about something
            •	Help asylum-seeking children and young people contact with their family – if they wish to and it is safe for them to do so in liaison with their personal advisors/social workers.
            Our team will support them with:

            a)	Everyday budgeting
            b)	Shopping, cooking and nutrition
            c)	Cleaning and maintaining a home
            d)	Paying bills
            e)	Staying safe at home
            f)	Managing behaviour and being part of a community
            g)	Positive healthy life styles
            h)	Accessing services they might need e.g. medical (registered with GP/Dentist), legal, educational
            i)	Applying for National Insurance Number / Travel Documents / Passports for those who qualify.



            Planning and Support
            BravoLife will work with Local Authorities to deliver asylum-seeking children and young people Pathway Plans, which will set out their plans for the future. This is an important legal document and we will make sure that the views and aspirations of care leavers must be at the centre of it. We will support them with parallel/triple planning, that is if they are granted leave to remain, or refused and are appealing the decision, or are appeals right exhausted and have been asked to leave the United Kingdom.  The possibility of return for some asylum-seeking young people will be discussed as part of the pathway planning process, where there is a possibility of them becoming Appeals Rights Exhausted.
            We will work closely with social workers to complete needs assessment report setting out their abilities, their achievements and their needs now and in the future.
            We will endeavour to deliver the Pathway Plan, which will set out how they will be supported to achieve the things that they want for themselves in their life, and who is going to provide the help they need.

            Financial Matters
            We will support the asylum-seeking children and young people understand the several different sources of money they are entitled to as a care leaver (the amounts will vary depending on their circumstances and legal status). Other factors that may affect payments made to them will include the type of accommodation they live in and whether they are engaged in employment, education or training.
            We will have regard to any financial support set out clearly in their Pathway Plans. We will also support them understand the Care Leavers’ Financial Entitlements.
            We will support asylum-seeking 18 years + young people benefits and ensure they get everything they are entitled to.

            Benefits claims
            •	BravoLife will help asylum-seeking young people (those with leave to remain or have been granted) with their initial claim, and help them with their applications. We will support them secure identification documents and a bank account for the benefits to be paid into. We will work with their social workers/personal advisors to help them get these in place before their 18th birthday, and support them to apply for their benefits before their birthday so that everything is set up to reduce any delays in payments.


            Job Seeker’s Allowance (JSA)
            •	We will support young people (those granted leave to remain) who are unemployed, available for and actively seeking work, working less than 16 hours per week on average. We will support them actively looked for work and prepare for job interviews, or attend appointments at short notice.

            Housing Benefit
            •	We will support young people (those granted leave to remain) apply for housing benefit. If they are likely to have problems paying their full rent, we will support them apply for Discretionary Housing Payments.

            Income Support
            •	We will support young people (those granted leave to remain) to apply for Income support if they are entitled to apply.

            Setting up home
            We will support young people (those granted leave to remain) with setting up home allowance. We will support them to move into independent living arrangements and support them to buy the essentials required for their new home. We will work closely with their social workers/personal advisors on this.

            Care Leavers in Higher Education (University)
            We will support care leavers to apply for student loans and grants to Student Finance. We will also support care leavers apply for bursaries to Buttle UK, a charity which awards universities a quality mark for offering additional support to care leavers.

            Disabled or ill care leavers
            We will support young people (those granted leave to remain) to apply for Employment and Support Allowance (ESA) – a benefit for sick or disabled people who cannot work.
            We will support care leavers to apply for Personal Independence Payment (PIP) – helps with some of the extra costs caused by long-term ill health or disability.



            Education, Training and Employment
            We will encourage and support young people (those granted leave to remain) to make education a top priority and we will support them to stay in education, whether that is going to college, starting an apprenticeship, going to university or finding a job.
            We have specialist keyworkers who can help care leavers explore their options and help them make applications and support them once they start education or work.
            We will support them practically and financially as set out above. The ways we will support them will be agreed in their Pathway Plan.

            Advocacy
            We will support asylum seeking children and young people someone to help represent their views.  We will provide an independent advocate to ensure that they are listened to and help them get their views across.'
        ]);
    }
}