<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CompanySetting;
use App\Mail\newsletterMail;
use Illuminate\Support\Facades\Mail;

class NewsLetter extends Model
{
    protected $fillable = ['title', 'message'];

    public function createNewsLetter($title, $message)
    {
        $this::create([
            'title' => $title,
            'message' => $message
        ]);
    }

    public function updateNewsLetter($id, $title, $message)
    {
        $newsletter = $this::find($id);
        $newsletter->title = $title;
        $newsletter->message = $message;
        $newsletter->save();
    }

    public function sendNewsLetter($id)
    {
        $newsletter = $this::find($id);
        $registeredUser = RegisteredUser::all();
        $setting = CompanySetting::first();
        if (count($registeredUser) > 0) {
            foreach ($registeredUser as $user) {
                Mail::to($user)->send(new newsletterMail($newsletter, $setting));
            }
            return true;
        }
        return false;
    }

    public function destroyNewsletter($id)
    {
        $this::find($id)->delete();
    }
}