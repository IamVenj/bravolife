<?php

namespace App;

use App\Mail\ContactMail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class Contact extends Model
{
    protected $fillable = ['name', 'email', 'subject', 'message'];

    // public function getContact

    public function sendMessage($name, $email, $subject, $message)
    {
        $contact = $this::create([
            'name' => $name,
            'email' => $email,
            'subject' => $subject,
            'message' => $message
        ]);
        Mail::bcc($contact->email)->send(new ContactMail($contact));
    }

    public function destroyMessage($id)
    {
        $this::find($id)->delete();
    }
}