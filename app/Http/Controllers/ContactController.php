<?php

namespace App\Http\Controllers;

use App\CompanySetting;
use Illuminate\Http\Request;
use App\Contact;

class ContactController extends Controller
{
    private $_contact, $_setting;
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'store');
        $this->_contact = new Contact();
        $this->_setting = new CompanySetting();
    }

    public function index()
    {
        $setting = $this->_setting->getSettings();
        return view('pre.pages.contact', compact('setting'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'subject' => 'required',
            'message' => 'required'
        ]);
        $this->_contact->sendMessage($request->name, $request->email, $request->subject, $request->message);
        return response()->json(['message' => 'Message is successfully sent!'], 200);
    }

    public function adminIndex()
    {
        return view('post.pages.contact.index');
    }

    public function getContactData()
    {
        return $this->_contact::latest()->paginate(10);
    }

    public function destroy($id)
    {
        $this->_contact->destroyMessage($id);
    }
}