<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Carousel;

class CarouselController extends Controller
{
    private $_carousel;

    public function __construct()
    {
        $this->middleware('auth');
        $this->_carousel = new Carousel();
    }

    public function index()
    {
        return view('post.pages.customize.carousel.carousel');
    }

    public function getPaginatedCarousel()
    {
        return $this->_carousel::latest()->paginate(6);
    }

    public function create()
    {
        return view('post.pages.customize.carousel.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'carouselImage' => 'required',
            'carouselTitle' => 'required',
            'carouselSlug' => 'required'
        ]);
        $this->_carousel->createCarousel($request->carouselImage, $request->carouselTitle, $request->carouselSlug);
        return response()->json(["message" => "carousel is successfully created!"], 200);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'carouselTitle' => 'required',
            'carouselSlug' => 'required'
        ]);
        $this->_carousel->updateCarousel($id, $request->carouselTitle, $request->carouselSlug);
        return response()->json(["message" => "carousel is successfully updated!"]);
    }

    public function updateImage(Request $request, $id)
    {
        $request->validate([
            'image' => 'required',
        ]);
        $this->_carousel->updateCarouselImage($id, $request->image);
        return response()->json(["message" => "Carousel Image is successfully updated!"]);
    }

    public function destroy($id)
    {
        $this->_carousel->destroyCarousel($id);
        return response()->json(["message" => "Carousel is successfully deleted!"], 200);
    }
}