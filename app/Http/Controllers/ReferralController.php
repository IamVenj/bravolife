<?php

namespace App\Http\Controllers;

use App\Referral;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReferralController extends Controller
{
    private $referral;
    public function __construct()
    {
        $this->middleware('auth')->except('store');
        $this->referral = new Referral();
    }

    public function index()
    {
        return view('post.pages.referral.index');
    }

    public function indexArchive()
    {
        return view('post.pages.referral.archive');
    }

    public function getUnArchivedReferral()
    {
        return $this->referral->getReferrals();
    }

    public function getArchivedReferral()
    {
        return $this->referral->getArchivedReferrals();
    }

    public function store(Request $request)
    {
        $request->validate([
            'dateofbirth' => 'required',
            'surname' => 'required',
            'firstname' => 'required',
            'email' => 'required',
            'contact_tel' => 'required',
            'address' => 'required',
            'organization' => 'required',
            'referrer_name' => 'required',
            'disability_needs' => 'required',
            'age' => 'required',
            'placeofbirth' => 'required'
        ]);

        $dateOfBirth = Carbon::createFromDate(
            $request->dateofbirth
        );

        $this->referral->createReferral(
            $request->referrer_name,
            $request->organization,
            $request->address,
            $request->email,
            $request->contact_tel,
            $request->firstname,
            $request->surname,
            $dateOfBirth,
            $request->age,
            $request->placeofbirth,
            $request->disability_needs
        );
        return response()->json(['message' => 'Referral is successfully sent!'], 200);
    }

    public function archive($id)
    {
        $this->referral->archiveReferral($id);
        return response()->json(['message' => 'Referral is successfully archived!'], 200);
    }

    public function unArchive($id)
    {
        $this->referral->unArchiveReferral($id);
        return response()->json(['message' => 'Referral is successfully unArchived!'], 200);
    }
}