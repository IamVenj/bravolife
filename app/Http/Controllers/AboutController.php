<?php

namespace App\Http\Controllers;

use App\CompanySetting;
use Illuminate\Http\Request;
use App\CustomAbout;

class AboutController extends Controller
{
    private $_about, $_settings;
    public function __construct()
    {
        $this->middleware('auth')->except('index');
        $this->_about = new CustomAbout();
        $this->_settings = new CompanySetting();
    }

    public function index()
    {
        $about = $this->_about->getAbout();
        $setting = $this->_settings->getSettings();
        return view('pre.pages.about', compact('about', 'setting'));
    }

    public function adminIndex()
    {
        return view('post.pages.customize.about.index');
    }

    public function getAboutData()
    {
        return $this->_about::first();
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'mainSlug' => 'required',
            'ourVision' => 'required',
            'ourMission' => 'required'
        ]);
        $this->_about->updateAbout(
            $request->mainSlug,
            $request->ourVision,
            $request->ourMission,
            $request->image,
            $id
        );
        return response()->json(['message' => "About is successfully customized!"], 200);
    }
}