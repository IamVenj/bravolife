<?php

namespace App\Http\Controllers;

use App\CompanySetting;
use Illuminate\Http\Request;
use App\Event;
use Carbon\Carbon;

class EventController extends Controller
{
    private $_event, $_setting;
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
        $this->_event = new Event();
        $this->_setting = new CompanySetting();
    }

    public function index()
    {
        $events = $this->_event->getEvents();
        $setting = $this->_setting->getSettings();
        return view('pre.pages.events', compact('events', 'setting'));
    }

    public function show()
    {
        return view('pre.pages.thisEvent');
    }

    public function adminIndex()
    {
        return view('post.pages.event.index');
    }

    public function getEvents()
    {
        return $this->_event::latest()->paginate(6);
    }

    public function create()
    {
        return view('post.pages.event.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'image' => 'required',
            'slug' => 'required',
            'date' => 'required',
            'location' => 'required'
        ]);
        $date = Carbon::createFromDate($request->date);
        $this->_event->createEvent(
            $request->title,
            $request->image,
            $request->slug,
            $date,
            $request->startTime,
            $request->endTime,
            $request->location
        );
        return response()->json(['message' => 'Event is successfully created!']);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'slug' => 'required',
            'date' => 'required',
        ]);
        $date = Carbon::createFromDate($request->date);
        $this->_event->updateEvent(
            $id,
            $request->title,
            $request->slug,
            $date,
            $request->startTime,
            $request->endTime,
            $request->location
        );
        return response()->json(['message' => 'Event is successfully updated!'], 200);
    }

    public function updateImage(Request $request, $id)
    {
        $request->validate([
            'image' => 'required'
        ]);
        $this->_event->updateEventImage($id, $request->image);
        return response()->json(['message' => 'Event Image is successfully updated!'], 200);
    }

    public function destroy($id)
    {
        $this->_event->destroyEvent($id);
        return response()->json(['message' => 'Event is successfully deleted!'], 200);
    }
}