<?php

namespace App\Http\Controllers;

use App\NewsLetter;
use App\RegisteredUser;
use Illuminate\Http\Request;

class NewsLetterController extends Controller
{
    private $_registeredUser, $_newsletter;
    public function __construct()
    {
        $this->middleware('auth')->except('subscribe');
        $this->_registeredUser = new RegisteredUser();
        $this->_newsletter = new NewsLetter();
    }

    public function subscribe(Request $request)
    {
        $request->validate([
            'email' => 'required|unique:registered_users'
        ]);
        $this->_registeredUser->registerUser($request->email);
        return response()->json(['message' => 'You are successfully registered!'], 200);
    }

    public function index()
    {
        return view('post.pages.newsletter.create');
    }

    public function registeredIndex()
    {
        return view('post.pages.newsletter.users');
    }

    public function vueIndex()
    {
        return $this->_newsletter::latest()->paginate(5);
    }

    public function getUser()
    {
        return $this->_registeredUser::latest()->paginate(10);
    }

    public function search(Request $request)
    {
        $request->validate([
            'searchquery' => 'required'
        ]);
        $searchResult = $this->_newsletter::where('title', "LIKE", '%' . strtolower($request->searchquery) . '%')->orWhere('message', "LIKE", '%' . strtolower($request->searchquery) . '%')->latest()->paginate(5);
        return $searchResult;
    }

    public function searchUsers(Request $request)
    {
        $request->validate([
            'searchquery' => 'required'
        ]);
        $searchResult = $this->_registeredUser::where('email', "LIKE", '%' . strtolower($request->searchquery) . '%')->latest()->paginate(10);
        return $searchResult;
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'message' => 'required'
        ]);
        $this->_newsletter->createNewsLetter($request->title, $request->message);
        return response()->json(['message' => "Newsletter is successfully created!"], 200);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'message' => 'required'
        ]);
        $this->_newsletter->updateNewsLetter($id, $request->title, $request->message);
        return response()->json(['message' => "Newsletter is successfully updated!"], 200);
    }

    public function send($id)
    {
        $send = $this->_newsletter->sendNewsLetter($id);
        if ($send) return response()->json(['message' => "Newsletter is successfully sent!"], 200);
        return response()->json(['message' => "Sorry! No One has subscribed to your newsletter!"], 200);
    }

    public function destroy($id)
    {
        $this->_newsletter->destroyNewsletter($id);
        return response()->json(['message' => "Newsletter is successfully deleted!"], 200);
    }
}