<?php

namespace App\Http\Controllers;

use App\CompanySetting;
use App\Gallery;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    private $_gallery, $_settings;
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'getGallery', 'getHomeGallery');
        $this->_gallery = new Gallery();
        $this->_settings = new CompanySetting();
    }

    /**
     * This is for the client / PRE
     * @return view
     */
    public function index()
    {
        $setting = $this->_settings->getSettings();
        return view('pre.pages.Gallery', compact('setting'));
    }

    /**
     * VUE function
     * retrieve paginated gallery
     * -----------------------------
     * @return object
     */
    public function getGallery()
    {
        return $this->_gallery->recoupAllImages();
    }

    public function getHomeGallery()
    {
        return $this->_gallery->getImagesForHomePage();
    }

    /**
     * This is for the admin / POST
     * @return view
     */
    public function adminIndex()
    {
        return view('post.pages.gallery.index');
    }


    /**
     * Create page
     * @return view
     */
    public function create()
    {
        return view('post.pages.gallery.create');
    }


    /**
     * store all images by bringing them as an array
     * @param object $request
     * @return object
     */
    public function store(Request $request)
    {
        $request->validate([
            'images' => 'required|array',
            'images.*' => 'required'
        ]);
        $this->_gallery->addImages($request->images);
        return response()->json([
            'message' => 'Images is successfully added!'
        ], 200);
    }

    /**
     * update a single images
     * @param object $request
     * @param int $id
     * @return object
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'image' => 'required'
        ]);
        $this->_gallery->updateImage($request->image, $id);
        return response()->json(['message' => 'Images is successfully updated!'], 200);
    }

    /**
     * destroy a single image
     * @param int $id
     * @return object
     */
    public function destroy($id)
    {
        $this->_gallery->destroyImage($id);
        return response()->json(['message' => 'Images is successfully deleted!'], 200);
    }
}