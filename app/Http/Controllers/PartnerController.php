<?php

namespace App\Http\Controllers;

use App\Partners;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    private $_partner;
    public function __construct()
    {
        $this->middleware('auth');
        $this->_partner = new Partners();
    }

    public function index()
    {
        return view('post.pages.partner.index');
    }

    public function create()
    {
        return view('post.pages.partner.create');
    }

    public function getPartners()
    {
        return $this->_partner::latest()->paginate(12);
    }

    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required'
        ]);
        $this->_partner->createPartner($request->image);
        return response()->json(['message' => 'Partner is created Successfully!'], 200);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'image' => 'required'
        ]);
        $this->_partner->updatePartner($id, $request->image);
        return response()->json(['message' => 'Partner is updated successfully!'], 200);
    }

    public function destroy($id)
    {
        $this->_partner->destroyPartner($id);
        return response()->json(['message' => 'Partner is deleted successfully!'], 200);
    }
}