<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('destroy');
    }

    public function index()
    {
        return view("post.auth.auth");
    }

    public function validateRequest()
    {
        $this->validate(request(), [
            "email" => 'required',
            'password' => 'required'
        ]);
    }

    protected function guard()
    {
        return \Auth::guard();
    }

    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    protected function credentials(Request $request)
    {
        return $request->only('email', 'password');
    }

    public function store(Request $request)
    {
        $this->validateRequest();
        if (!$this->attemptLogin($request)) {
            return response()->json(["status"=>"login_failed", "message"=>'Please Check Your credentials and Try again.'], 200);
        } else {
            return response()->json(["status"=>"success"], 200);
        }
    }

    public function destroy(Request $request)
    {
        $this->guard()->logout();
        $request->session()->invalidate();
        return redirect('/admin-login');
    }
}