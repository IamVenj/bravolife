<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    private $_user;
    public function __construct()
    {
        $this->middleware('auth');
        $this->_user = new User();
    }

    public function index()
    {
        return view('post.pages.profile.index');
    }

    public function getAuthUser()
    {
        return Auth::user();
    }

    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required'
        ]);
        $this->_user->updateAdminAccount($request->name, $request->email);
        return response()->json(['message' => 'Your Account is successfully updated!'], 200);
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'currentPassword' => 'required',
            'newPassword' => 'required'
        ]);
        $change = $this->_user->changePassword($request->currentPassword, $request->newPassword);
        if ($change) return response()->json(['message' => 'Your password is successfully updated!'], 200);
        return response()->json(['message' => 'Your current password doesn\'t match with the data on our database!'], 200);
    }
}