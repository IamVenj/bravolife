<?php

namespace App\Http\Controllers;

use App\Accomodation;
use App\AccomodationImages;
use App\CompanySetting;
use Illuminate\Http\Request;

class AccommodationController extends Controller
{
    private $_accommodation, $_settingAccommodation, $_accommodationImage, $_setting;
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'getAccommodation');
        $this->_accommodation = new Accomodation();
        $this->_settingAccommodation = new CompanySetting();
        $this->_accommodationImage = new AccomodationImages();
    }

    public function index()
    {
        $setting = $this->_settingAccommodation->getSettings();
        return view('pre.pages.accomodation', compact('setting'));
    }

    public function getAccommodation()
    {
        $mainAccommodation = $this->_settingAccommodation->getSettings()->accommodation;
        $accommodations = $this->_accommodation->getAccommodation();
        return response()->json(['accommodations' => $accommodations, 'mainAccommodation' => $mainAccommodation], 200);
    }

    public function adminIndex()
    {
        return view('post.pages.accommodation.index');
    }

    public function create()
    {
        return view('post.pages.accommodation.create');
    }

    public function showImage($id)
    {
        return view('post.pages.accommodation.showImage', compact('id'));
    }

    public function getImages($id)
    {
        return $this->_accommodationImage::where('accomodation_id', $id)->get();
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'slug' => 'required',
            'images.*' => 'required|array'
        ]);

        $accommodation = $this->_accommodation->addAccommodation($request->title, $request->slug);
        $this->_accommodationImage->addImages($accommodation->id, $request->images);

        return response()->json(['message' => 'Accommodation is successfully created!'], 200);
    }

    public function updateAccommodationMain(Request $request)
    {
        $request->validate([
            'accommodation_main_slug' => 'required'
        ]);
        $this->_settingAccommodation->updateAccommodationMain($request->accommodation_main_slug);
        return response()->json(['message' => 'Accommodation is successfully updated!'], 200);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'accommodationTitle' => 'required',
            'accommodationSlug' => 'required'
        ]);

        $this->_accommodation->updateAccommodation(
            $id,
            $request->accommodationTitle,
            $request->accommodationSlug
        );
        return response()->json(['message' => 'Accommodation is successfully updated!'], 200);
    }

    public function updateImage(Request $request, $id)
    {
        $request->validate([
            'image' => 'required'
        ]);
        $this->_accommodationImage->updateImage($id, $request->image);
        return response()->json(['message' => 'Accommodation Image is successfully updated!'], 200);
    }

    public function destroyImage($id)
    {
        $this->_accommodationImage->destroyImage($id);
        return response()->json(['message' => 'Accommodation Image is successfully deleted!'], 200);
    }

    public function destroy($id)
    {
        $this->_accommodation->destroyAccommodation($id);
        return response()->json(['message' => 'Accommodation is successfully deleted!'], 200);
    }
}