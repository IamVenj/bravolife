<?php

namespace App\Http\Controllers;

use App\CompanySetting;
use App\Help;
use Illuminate\Http\Request;

class HelpController extends Controller
{
    private $_help, $_settings;
    public function __construct()
    {
        $this->middleware('auth')->except('index');
        $this->_help = new Help();
        $this->_settings = new CompanySetting();
    }

    public function index()
    {
        $help = $this->getHelp();
        $setting = $this->_settings->getSettings();
        return view('pre.pages.help', compact('help', 'setting'));
    }

    public function getHelp()
    {
        return $this->_help->getMyHelp();
    }

    public function adminIndex()
    {
        return view('post.pages.help.index');
    }

    public function store(Request $request)
    {
        $request->validate([
            'slug' => 'required'
        ]);
        $this->_help->updateHelp($request->slug);
        return response()->json(['message' => 'Help and support is successfully updated!'], 200);
    }
}