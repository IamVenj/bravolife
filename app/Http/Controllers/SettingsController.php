<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CompanySetting;

class SettingsController extends Controller
{
    private $_setting;
    public function __construct()
    {
        $this->middleware('auth');
        $this->_setting = new CompanySetting();
    }

    public function index()
    {
        return view('post.pages.settings.index');
    }

    public function getSettings()
    {
        return $this->_setting::first();
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'location' => 'required',
            'phone_number' => 'required',
        ]);

        $this->_setting->updateSetting(
            $id,
            $request->name,
            $request->email,
            $request->location,
            $request->phone_number,
            $request->twitter,
            $request->facebook,
            $request->linked_in,
            $request->google_plus,
            $request->imageUrl
        );
        return response()->json(['message' => "Settings is successfully customized!"], 200);
    }
}