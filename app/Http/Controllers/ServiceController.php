<?php

namespace App\Http\Controllers;

use App\CompanySetting;
use App\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    private $_service, $_settings;
    public function __construct()
    {
        $this->middleware('auth')->except('index');
        $this->_service = new Service();
        $this->_settings = new CompanySetting();
    }

    public function index()
    {
        $setting = $this->_settings->getSettings();
        $service = $this->_service->getServices();
        return view('pre.pages.service', compact('setting', 'service'));
    }

    public function adminIndex()
    {
        return view('post.pages.service.index');
    }

    // public function create()
    // {
    //     return view('post.pages.service.create');
    // }

    // public function getPaginatedService()
    // {
    //     return $this->_service::latest()->paginate(9);
    // }

    public function store(Request $request)
    {
        $request->validate([
            'slug' => 'required'
        ]);
        $this->_service->updateService($request->slug);
        return response()->json(['message' => 'Service is successfully updated!'], 200);
    }

    public function getService()
    {
        return $this->_service::first();
    }

    // public function update(Request $request, $id)
    // {
    //     $request->validate([
    //         'language' => 'required',
    //         'title' => 'required',
    //         'slug' => 'required'
    //     ]);
    //     $this->_service->updateService($request->language, $request->title, $request->slug, $id);
    //     return response()->json(['message' => 'Service is successfully updated!'], 200);
    // }

    // public function updateImage(Request $request, $id)
    // {
    //     $request->validate([
    //         'image' => 'required'
    //     ]);
    //     $this->_service->updateServiceImage($request->image, $id);
    //     return response()->json(['message' => 'Service Image is successfully updated!'], 200);
    // }

    // public function destroy($id)
    // {
    //     $this->_service->destroyService($id);
    //     return response()->json(['message' => 'Service is successfully deleted!'], 200);
    // }
}