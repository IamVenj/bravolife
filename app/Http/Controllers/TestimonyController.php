<?php

namespace App\Http\Controllers;

use App\Testimony;
use Illuminate\Http\Request;

class TestimonyController extends Controller
{
    private $_testimony;
    public function __construct()
    {
        $this->middleware('auth');
        $this->_testimony = new Testimony();
    }

    public function index()
    {
        return view('post.pages.testimony.index');
    }

    public function getTestimony()
    {
        return $this->_testimony::latest()->paginate(6);
    }

    public function create()
    {
        return view('post.pages.testimony.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'position' => 'required',
            'image' => 'required',
            'testimony' => 'required'
        ]);
        $this->_testimony->createTestimony($request->name, $request->position, $request->image, $request->testimony);
        return response()->json(['message' => 'Testimony is successfully created!'], 200);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'position' => 'required',
            'testimony' => 'required'
        ]);
        $this->_testimony->updateTestimony($id, $request->name, $request->position, $request->testimony);
        return response()->json(['message' => 'Testimony is successfully updated!'], 200);
    }

    public function updateImage(Request $request, $id)
    {
        $request->validate([
            'image' => 'required'
        ]);
        $this->_testimony->updateProfileImage($id, $request->image);
        return response()->json(['message' => 'Testimony image is successfully updated!'], 200);
    }

    public function destroy($id)
    {
        $this->_testimony->destroyTestimony($id);
        return response()->json(['message' => 'Testimony is successfully deleted!'], 200);
    }
}