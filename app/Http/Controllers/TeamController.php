<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;

class TeamController extends Controller
{
    private $_team;
    public function __construct()
    {
        $this->_team = new Team();
        $this->middleware('auth');
    }

    public function index()
    {
        return view('post.pages.team.index');
    }

    public function create()
    {
        return view('post.pages.team.create');
    }

    public function getTeamMembers()
    {
        return $this->_team::latest()->paginate(9);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'position' => 'required',
            'slug' => 'required',
            'image' => 'required'
        ]);
        $this->_team->createTeamMember(
            $request->name,
            $request->position,
            $request->slug,
            $request->facebook,
            $request->twitter,
            $request->linkedIn,
            $request->image
        );
        return response()->json(['message' => 'Team Member is successfully created!'], 200);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'position' => 'required',
            'slug' => 'required'
        ]);
        $this->_team->updateTeamMember(
            $id,
            $request->name,
            $request->position,
            $request->slug,
            $request->facebook,
            $request->twitter,
            $request->linkedIn
        );
        return response()->json(['message' => 'Team Member is successfully updated!'], 200);
    }

    public function updateImage($id, Request $request)
    {
        $request->validate([
            'image' => 'required'
        ]);
        $this->_team->updateTeamImage($id, $request->image);
        return response()->json(['message' => 'Team Member Image is successfully updated!'], 200);
    }

    public function destroy($id)
    {
        $this->_team->destroyTeam($id);
        return response()->json(['message' => 'Team Member is successfully deleted!'], 200);
    }
}