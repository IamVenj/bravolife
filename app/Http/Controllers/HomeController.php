<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Carousel;
use App\CompanySetting;
use App\CustomAbout;
use App\Event;
use App\Partners;
use App\Service;
use App\Testimony;

class HomeController extends Controller
{
    private $carousel, $about, $event, $partner, $testimony, $service, $setting;
    public function __construct()
    {
        $this->carousel = new Carousel();
        $this->about = new CustomAbout();
        $this->event = new Event();
        $this->partner = new Partners();
        $this->testimony = new Testimony();
        $this->service = new Service();
        $this->setting = new CompanySetting();
    }

    public function index()
    {
        $carouselz = $this->carousel->getAllCarousels();
        $aboutFirst = $this->about->getAbout();
        $events = $this->event->getEventForHomePage();
        $partners = $this->partner->getPartners();
        $testimonies = $this->testimony->getTestimonies();
        $services = $this->service->getServices();
        $settings = $this->setting->getSettings();
        return view('pre.pages.home', compact('carouselz', 'aboutFirst', 'events', 'partners', 'testimonies', 'services', 'settings'));
    }
}