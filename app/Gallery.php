<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable = ['image'];

    public function recoupAllImages()
    {
        return $this::latest()->paginate(8);
    }

    public function getImagesForHomePage()
    {
        return $this::latest()->take(4)->get();
    }

    public function addImages($images)
    {
        for ($i = 0; $i < count($images); $i++) {
            $this::create([
                'image' => $images[$i]['signedUrl']
            ]);
        }
    }

    public function addImage($image)
    {
        $this::create([
            'image' => $image,
        ]);
    }

    public function updateImage($image, $id)
    {
        $gallery = $this::find($id);
        $gallery->image = $image;
        $gallery->save();
    }

    public function destroyImage($id)
    {
        $this::find($id)->delete();
    }
}