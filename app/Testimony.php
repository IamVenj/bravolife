<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimony extends Model
{
    protected $fillable = ['name', 'position', 'image_url', 'testimony'];

    public function getTestimonies()
    {
        return $this->latest()->get();
    }

    public function createTestimony($name, $position, $imageUrl, $testimony)
    {
        $this::create([
            'name' => $name,
            'position' => $position,
            'image_url' => $imageUrl,
            'testimony' => $testimony
        ]);
    }

    public function updateTestimony($id, $name, $position, $testimonyItself)
    {
        $testimony = $this::find($id);
        $testimony->name = $name;
        $testimony->position = $position;
        $testimony->testimony = $testimonyItself;
        $testimony->save();
    }

    public function updateProfileImage($id, $imageUrl)
    {
        $testimony = $this::find($id);
        $testimony->image_url = $imageUrl;
        $testimony->save();
    }

    public function destroyTestimony($id)
    {
        $this::find($id)->delete();
    }
}