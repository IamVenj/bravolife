<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanySetting extends Model
{
    protected $fillable = [
        'name', 'email', 'location', 'phone_number', 'twitter', 'facebook', 'linked_in', 'google_plus', 'image_url', 'accommodation'
    ];

    public function getSettings()
    {
        return $this->first();
    }

    public function updateAccommodationMain($accommodation)
    {
        $setting = $this::first();
        $setting->accommodation = $accommodation;
        $setting->save();
    }

    public function updateSetting($id, $name, $email, $location, $phoneNumber, $twitter, $facebook, $linked_in, $google_plus, $imageUrl)
    {
        $setting = $this::find($id);
        $setting->name = $name;
        $setting->email = $email;
        $setting->location = $location;
        $setting->phone_number = $phoneNumber;
        $setting->twitter = $twitter;
        $setting->facebook = $facebook;
        $setting->linked_in = $linked_in;
        $setting->google_plus = $google_plus;
        $setting->image_url = $imageUrl;
        $setting->save();
    }
}