<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = ['title', 'image_url', 'slug', 'date', 'start_time', 'end_time', 'location'];

    public function getEventForHomePage()
    {
        return $this->latest()->take(3)->get();
    }

    public function getEvents()
    {
        return $this->latest()->paginate(9);
    }

    public function createEvent($title, $imageUrl, $slug, $date, $startTime, $endTime, $location)
    {
        $this::create([
            'title' => $title,
            'image_url' => $imageUrl,
            'slug' => $slug,
            'date' => $date,
            'start_time' => $startTime,
            'end_time' => $endTime,
            'location' => $location
        ]);
    }

    public function updateEvent($id, $title, $slug, $date, $startTime, $endTime, $location)
    {
        $event = $this::find($id);
        $event->title = $title;
        $event->slug = $slug;
        $event->date = $date;
        $event->start_time = $startTime;
        $event->end_time = $endTime;
        $event->location = $location;
        $event->save();
    }

    public function updateEventImage($id, $image)
    {
        $event = $this::find($id);
        $event->image_url = $image;
        $event->save();
    }

    public function destroyEvent($id)
    {
        $this::find($id)->delete();
    }
}