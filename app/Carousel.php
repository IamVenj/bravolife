<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carousel extends Model
{
    protected $fillable = ['image_url', 'title', 'slug'];

    public function getAllCarousels()
    {
        return $this::latest()->get();
    }

    public function createCarousel($image, $title, $slug)
    {
        $this::create([
            'image_url' => $image,
            'title' => $title,
            'slug' => $slug
        ]);
    }

    public function updateCarouselImage($id, $image)
    {
        $carousel = $this::find($id);
        $carousel->image_url = $image;
        $carousel->save();
    }

    public function updateCarousel($id, $title, $slug)
    {
        $carousel = $this::find($id);
        $carousel->title = $title;
        $carousel->slug = $slug;
        $carousel->save();
    }

    public function destroyCarousel($id)
    {
        $this::find($id)->delete();
    }
}