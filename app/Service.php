<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = ['service'];

    public function getServices()
    {
        return $this::first();
    }

    public function createService($service)
    {
        $this::create([
            "service" => $service
        ]);
    }

    public function updateService($slug)
    {
        $service = $this::first();
        $service->service = $slug;
        $service->save();
    }

    public function updateServiceImage($image, $id)
    {
        $service = $this::find($id);
        $service->image = $image;
        $service->save();
    }

    public function destroyService($id)
    {
        $this::find($id)->delete();
    }
}