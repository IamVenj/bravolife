<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function updateAdminAccount($name, $email)
    {
        $admin = $this::find(Auth::user()->id);
        $admin->name = $name;
        $admin->email = $email;
        $admin->save();
    }

    public function changePassword($currentPassword, $newPassword)
    {
        if (Hash::check($currentPassword, Auth::user()->password)) {
            $admin = $this::find(Auth::user()->id);
            $admin->password = bcrypt($newPassword);
            $admin->save();
            return true;
        }
        return false;
    }
}