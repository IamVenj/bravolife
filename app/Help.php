<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Help extends Model
{
    protected $fillable = ['slug'];

    public function getMyHelp()
    {
        return $this::first();
    }

    public function updateHelp($slug)
    {
        $help = $this::first();
        $help->slug = $slug;
        $help->save();
    }
}