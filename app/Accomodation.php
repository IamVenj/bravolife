<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accomodation extends Model
{
    protected $fillable = ['name', 'slug'];

    public function images()
    {
        return $this->hasMany(AccomodationImages::class, 'accomodation_id', 'id');
    }

    public function getAccommodation()
    {
        return $this::with(['images'])->get();
    }

    public function addAccommodation($name, $slug)
    {
        $accommodation = $this::create([
            'name' => $name,
            'slug' => $slug
        ]);
        return $accommodation;
    }

    public function updateAccommodation($id, $name, $slug)
    {
        $acc = $this::find($id);
        $acc->name = $name;
        $acc->slug = $slug;
        $acc->save();
    }

    public function destroyAccommodation($id)
    {
        $this::find($id)->delete();
    }
}