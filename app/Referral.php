<?php

namespace App;

use App\Mail\ReferralMail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class Referral extends Model
{
    protected $fillable = [
        'referrer_name', 'referrer_organization', 'referrer_address', 'referrer_email',
        'referrer_contact_tel', 'applicant_first_name', 'applicant_sur_name', 'applicant_date_of_birth',
        'applicant_age', 'applicant_place_of_birth', 'applicant_disability_need', 'archived'
    ];

    public function getReferrals()
    {
        return $this::latest()->where('archived', 0)->paginate(12);
    }

    public function getArchivedReferrals()
    {
        return $this::latest()->where('archived', 1)->paginate(12);
    }

    public function createReferral(
        $referrer_name,
        $referrer_organization,
        $referrer_address,
        $referrer_email,
        $referrer_contact_tel,
        $applicant_first_name,
        $applicant_sur_name,
        $applicant_date_of_birth,
        $applicant_age,
        $applicant_place_of_birth,
        $applicant_disability_need
    ) {
        $currentReferral = $this::create([
            'referrer_name' => $referrer_name,
            'referrer_organization' => $referrer_organization,
            'referrer_address' => $referrer_address,
            'referrer_email' => $referrer_email,
            'referrer_contact_tel' => $referrer_contact_tel,
            'applicant_first_name' => $applicant_first_name,
            'applicant_sur_name' => $applicant_sur_name,
            'applicant_date_of_birth' => $applicant_date_of_birth,
            'applicant_age' => $applicant_age,
            'applicant_place_of_birth' => $applicant_place_of_birth,
            'applicant_disability_need' => $applicant_disability_need
        ]);
        Mail::bcc($currentReferral->referrer_email)->send(new ReferralMail($currentReferral));
    }

    public function archiveReferral($id)
    {
        $referral = $this::find($id);
        $referral->archived = 1;
        $referral->save();
    }

    public function unArchiveReferral($id)
    {
        $referral = $this::find($id);
        $referral->archived = 0;
        $referral->save();
    }
}