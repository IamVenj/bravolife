<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisteredUser extends Model
{
    protected $fillable = ['email'];

    public function registerUser($email)
    {
        $this::create([
            'email' => $email
        ]);
    }
}