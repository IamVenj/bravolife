<?php

namespace App\Mail;

use App\Referral;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReferralMail extends Mailable
{
    use Queueable, SerializesModels;
    public $_referral;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Referral $_referral)
    {
        $this->_referral = $_referral;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Referral Mail from " . $this->_referral->referrer_email)
            ->from($this->_referral->referrer_email)
            ->to(env('MAIL_USERNAME'))
            ->view('pre.pages.partial.email.referralemail')->with('_referral');
    }
}