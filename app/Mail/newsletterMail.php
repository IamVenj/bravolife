<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\NewsLetter;
use App\CompanySetting;

class newsletterMail extends Mailable
{
    use Queueable, SerializesModels;
    public $_newsletter, $_settings;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(NewsLetter $_newsletter, CompanySetting $_settings)
    {
        $this->_newsletter = $_newsletter;
        $this->_settings = $_settings;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('post.pages.newsletter.mail.newsletter')->with('_settings', '_newsletter');
    }
}