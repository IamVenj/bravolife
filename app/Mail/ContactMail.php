<?php

namespace App\Mail;

use App\CompanySetting;
use App\Contact;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;
    public $contact;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return  $this->subject("Contact: " . $this->contact->subject)
            ->from($this->contact->email, $this->contact->email)
            ->to(env('MAIL_USERNAME'), 'BravoLife')
            ->view('pre.pages.partial.email.contactmail')->with('contact');
    }
}