<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partners extends Model
{
    protected $fillable = ['image_url'];

    public function getPartners()
    {
        return $this::latest()->get();
    }

    public function createPartner($imageUrl)
    {
        $this::create([
            'image_url' => $imageUrl
        ]);
    }

    public function updatePartner($id, $imageUrl)
    {
        $partner = $this::find($id);
        $partner->image_url = $imageUrl;
        $partner->save();
    }

    public function destroyPartner($id)
    {
        $this::find($id)->delete();
    }
}