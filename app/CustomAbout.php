<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomAbout extends Model
{
    protected $fillable = ['main_slug', 'our_vision', 'our_mission', 'image_url'];

    public function getAbout()
    {
        return $this::first();
    }

    public function updateAbout($mainSlug, $ourVision, $ourMission, $imageUrl, $id)
    {
        $about = $this::find($id);
        $about->main_slug = $mainSlug;
        $about->our_vision = $ourVision;
        $about->our_mission = $ourMission;
        $about->image_url = $imageUrl;
        $about->save();
    }
}