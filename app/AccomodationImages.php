<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccomodationImages extends Model
{
    protected $fillable = ['accomodation_id', 'image'];
    public function addImages($accomodation_id, $image)
    {
        for ($i = 0; $i < count($image); $i++) {
            $this::create([
                'image' => $image[$i]['signedUrl'],
                'accomodation_id' => $accomodation_id
            ]);
        }
    }

    public function updateImage($id, $image)
    {
        $accimage = $this::find($id);
        $accimage->image = $image;
        $accimage->save();
    }

    public function destroyImage($id)
    {
        $this::find($id)->delete();
    }
}