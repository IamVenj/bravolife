<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = ['name', 'position', 'slug', 'facebook', 'twitter', 'linkedin', 'image_url'];

    public function createTeamMember($name, $position, $slug, $facebook, $twitter, $linkedin, $image_url)
    {
        $this::create([
            'name' => $name,
            'position' => $position,
            'slug' => $slug,
            'facebook' => $facebook,
            'twitter' => $twitter,
            'linkedin' => $linkedin,
            'image_url' => $image_url
        ]);
    }

    public function updateTeamMember($id, $name, $position, $slug, $facebook, $twitter, $linkedIn)
    {
        $team = $this::find($id);
        $team->name = $name;
        $team->position = $position;
        $team->slug = $slug;
        $team->facebook = $facebook;
        $team->twitter = $twitter;
        $team->linkedin = $linkedIn;
        $team->save();
    }

    public function updateTeamImage($id, $imageUrl)
    {
        $team = $this::find($id);
        $team->image_url = $imageUrl;
        $team->save();
    }

    public function destroyTeam($id)
    {
        $this::find($id)->delete();
    }
}