<?php
$setting = App\CompanySetting::first();
?>
<header class="main-header">
    <!-- header-top -->
    <div class="header-top bg-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-12 column">
                    <div class="top-left">
                        <ul class="social-content">
                            @if(!is_null($setting->facebook) || !is_null($setting->twitter) ||
                            !is_null($setting->linked_in) || !is_null($setting->google_plus))
                            <li>Follow Us:</li>
                            @endif
                            @if(!is_null($setting->facebook))
                            <li><a href="{{ $setting->facebook }}"><i class="fa fa-facebook"></i></a></li>
                            @elseif(!is_null($setting->twitter))
                            <li><a href="{{ $setting->twitter }}"><i class="fa fa-twitter"></i></a></li>
                            @elseif(!is_null($setting->linked_in))
                            <li><a href="{{ $setting->linked_in }}"><i class="fa fa-linkedin"></i></a></li>
                            @elseif(!is_null($setting->google_plus))
                            <li><a href="{{ $setting->google_plus }}"><i class="fa fa-google-plus"></i></a></li>
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-8 col-sm-12 column">
                    <div class="top-right">
                        <ul class="right-content">
                            <li style="color: #fff;"><i class="fa fa-phone"></i>Call: {{ $setting->phone_number }}</li>
                            <li style="color: #fff;"><i class="fa fa-envelope"></i>Email: {{ $setting->email }}</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 clearfix column">
                    <ul class="nav-right">
                        <li class="donate-box">
                            <button class="donate-box-btn btn-referral">Referral form</button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div><!-- header-top end -->

    <!-- header-bottom -->
    <div class="header-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-12 col-sm-12 column">
                    <figure class="logo-box"><a href="/"><img src="{{ asset('client/images/bravolife.png') }}"
                                alt="bravolife" height="70px"></a></figure>
                </div>
                <div class="col-lg-9 col-md-12 col-sm-12 menu-column">
                    <div class="menu-area">
                        <nav class="main-menu navbar-expand-lg">
                            <div class="navbar-header">

                                <button type="button" class="navbar-toggle" data-toggle="collapse"
                                    data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                    <li @if(\Request::is('/')) class="current" @endif><a href="/">Home</a></li>
                                    <li @if(\Request::is('about')) class="current" @endif><a href="/about">About</a>
                                    </li>
                                    <li @if(\Request::is('service')) class="current" @endif><a
                                            href="/service">Services</a></li>

                                    <li @if(\Request::is('accommodation')) class="current" @endif><a
                                            href="/accommodation">Accommodation</a>
                                    </li>
                                    <li @if(\Request::is('help-and-support')) class="current" @endif><a
                                            href="/help-and-support">Help & Support</a>
                                    </li>
                                    <li @if(\Request::is('gallery')) class="current" @endif><a
                                            href="/gallery">Gallery</a>
                                    </li>
                                    <li @if(\Request::is('contact')) class="current" @endif><a
                                            href="/contact">Contact</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>

            </div>
        </div>
    </div><!-- header-bottom end -->


    <!--Sticky Header-->
    <div class="sticky-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-12 col-sm-12 column">
                    <figure class="logo-box"><a href="/"><img src="{{ asset('client/images/bravolife.png') }}"
                                height="40px" alt=""></a>
                    </figure>
                </div>
                <div class="col-lg-10 col-md-12 col-sm-12 menu-column">
                    <div class="menu-area">
                        <nav class="main-menu navbar-expand-lg">
                            <div class="navbar-header">
                                <!-- Toggle Button -->
                                <button type="button" class="navbar-toggle" data-toggle="collapse"
                                    data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                    <li @if(\Request::is('/')) class="current" @endif><a href="/">Home</a></li>
                                    <li @if(\Request::is('about')) class="current" @endif><a href="/about">About</a>
                                    </li>
                                    <li @if(\Request::is('service')) class="current" @endif><a
                                            href="/service">Services</a></li>

                                    <li @if(\Request::is('accommodation')) class="current" @endif><a
                                            href="/accommodation">Accommodation</a>
                                    </li>
                                    <li @if(\Request::is('help-and-support')) class="current" @endif><a
                                            href="/help-and-support">Help & Support</a>
                                    </li>
                                    <li @if(\Request::is('gallery')) class="current" @endif><a
                                            href="/gallery">Gallery</a>
                                    </li>
                                    <li @if(\Request::is('contact')) class="current" @endif><a
                                            href="/contact">Contact</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- sticky-header end -->
</header>
<!-- End Main Header -->
