<!--jquery js -->
<script src="{{ asset('client/js/jquery.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('client/js/bootstrap.js') }}"></script>
<script src="{{ asset('client/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('client/js/wow.js') }}"></script>
<script src="{{ asset('client/js/validation.js') }}"></script>
<script src="{{ asset('client/js/jquery.fancybox.pack.js') }}"></script>
<script type="text/javascript" src="{{ ('client/js/SmoothScroll.js') }}"></script>
{{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZQiiFTOGpm2qHVZmZY1s-aEnmHDhqKgk"></script> --}}
<script src="{{ asset('client/js/html5lightbox/html5lightbox.js') }}"></script>
{{-- <script src="{{ asset('client/js/gmaps.js') }}"></script> --}}
<script src="{{ asset('client/js/map-helper.js') }}"></script>
<script src="{{ asset('clientt/js/isotope.js') }}"></script>
<script src="{{ asset('client/js/jquery-ui.js') }}"></script>
<script src="{{ asset('client/js/jquery.appear.js') }}"></script>
<script src="{{ asset('client/js/jquery.countTo.js') }}"></script>
<!-- main-js -->
<script src="{{ asset('client/js/script.js') }}"></script>
