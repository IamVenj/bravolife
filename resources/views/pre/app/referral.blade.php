<div id="donate-popup" class="donate-popup">
    <div class="close-donate"><span class="fa fa-close"></span></div>
    <div class="popup-inner">
        <div class="container">
            <div class="donate-form-area">
                <h2>Referral Form</h2>
                <referral-component></referral-component>
            </div>
        </div>
    </div>
</div>
