<?php
$setting = App\CompanySetting::first();
?>
<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ $setting->name }}</title>

    <!-- Stylesheets -->
    <link href="{{ asset('client/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('client/css/responsive.css') }}" rel="stylesheet">
    <link rel="icon" href="{{ asset('admin/img/brand/favicon.png') }}" type="image/png">

</head>

<body class="boxed_wrapper">

    <div class="total-lds">
        <div class="lds-grid">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>

    @include('pre.app.nav')
    <div id="app">
        @yield('content')
        @include('pre.app.referral')
        @include('pre.app.footer')
    </div>
    <button class="scroll-top scroll-to-target" data-target="html">
        <span class="fa fa-long-arrow-up"></span>
    </button>

    @include('pre.app.js-scripts')

</body>

</html>
