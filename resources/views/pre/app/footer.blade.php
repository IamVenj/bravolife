<!-- subscribe-section -->
<subscription-component></subscription-component>
<!-- subscribe-section end -->
<?php
$setting = App\CompanySetting::first();
$about = App\CustomAbout::first();
?>

<!-- main-footer -->
<footer class="main-footer">
    <div class="container">
        <div class="footer-content">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12 footer-column">
                    <div class="logo-widget footer-widget">
                        <figure class="logo-box"><a href="/"><img src="{{ asset('client/images/footer-logo.png') }}"
                                    height="70px" alt=""></a>
                        </figure>
                        <div class="text">
                            <p>
                                <?= Str::limit($about->main_slug, 250); ?>
                            </p>
                        </div>
                        <ul class="footer-social">
                            @if(!is_null($setting->facebook))
                            <li><a href="{{ $setting->facebook }}"><i class="fa fa-facebook"></i></a></li>
                            @elseif(!is_null($setting->twitter))
                            <li><a href="{{ $setting->twitter }}"><i class="fa fa-twitter"></i></a></li>
                            @elseif(!is_null($setting->linked_in))
                            <li><a href="{{ $setting->linked_in }}"><i class="fa fa-linkedin"></i></a></li>
                            @elseif(!is_null($setting->google_plus))
                            <li><a href="{{ $setting->google_plus }}"><i class="fa fa-google-plus"></i></a></li>
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 offset-lg-2 footer-column">
                    <div class="service-widget footer-widget">
                        <div class="footer-title">Links</div>
                        <ul class="list">
                            <li><a href="/">Home</a></li>
                            <li><a href="/services">Services</a></li>
                            <li><a href="/about">About Us</a></li>
                            <li><a href="/accommodation">Accommodation</a></li>
                            <li><a href="/gallery">Gallery</a></li>
                            <li><a href="/contact">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 footer-widget">
                    <div class="contact-widget footer-widget">
                        <div class="footer-title">Contacts</div>
                        <div class="text">
                            <p>{{ $setting->location }}</p>
                            <p>{{ $setting->phone_number }}</p>
                            <p>{{ $setting->email }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- main-footer end -->


<!-- footer-bottom -->
<footer-bottom-component></footer-bottom-component>
<!-- footer-bottome end -->
