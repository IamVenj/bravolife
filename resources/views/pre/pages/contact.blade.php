@extends('pre.app.app')

@section('content')
<!-- page-title -->
<section class="page-title centred" style="background-image: url({{ $setting->image_url }});">
    <div class="container">
        <div class="content-box">
            <div class="title">Contact Us</div>
            <ul class="bread-crumb">
                <li><a href="/">Home</a></li>
                <li>Contact</li>
            </ul>
        </div>
    </div>
</section>
<!-- page-title end -->


<!-- contact-section -->
<section class="contact-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 contact-column">
                <div class="contact-info">
                    <div class="contact-title">Get In Touch</div>
                    <div class="row">
                        <div class="col-lg-6 col-md-12 col-sm-12 column">
                            <div class="left-column single-info centred">
                                <div class="icon-box"><i class="flaticon-map"></i></div>
                                <h5>Address</h5>
                                <div class="text">{{ $setting->location }}</div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 column">
                            <div class="right-column single-info">
                                <div class="icon-box"><i class="flaticon-mail"></i></div>
                                <h5>Email</h5>
                                <div class="text">{{ $setting->email }}</div>
                            </div>
                            <div class="right-column single-info">
                                <div class="icon-box"><i class="flaticon-phone-call-1"></i></div>
                                <h5>Phone No</h5>
                                <div class="text">{{ $setting->phone_number }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 contact-column">
                <contact-form-component></contact-form-component>
            </div>
        </div>
    </div>
</section>
@endsection
