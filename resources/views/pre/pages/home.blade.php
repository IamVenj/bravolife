@extends('pre.app.app')

@section('content')
<section class="main-slider">
    <div class="main-slider-carousel owl-carousel owl-theme slide-nav">
        @if(count($carouselz) > 0)
        @foreach($carouselz as $carousel)

        <div class="slide" style="background-image:url({{ $carousel->image_url }})">
            <div class="container">
                <div class="content">
                    <h1 style="width: 90%;">{{ $carousel->title }}</h1>
                    <div class="text carousel-description"><?= $carousel->slug; ?></div>
                </div>
            </div>
        </div>

        @endforeach

        @else

        <div class="slide"
            style="background-image:url(http://res.cloudinary.com/nfco/image/upload/v1580138494/phdmcr81ky1xraovzyt2.jpg)">
            <div class="container">
                <div class="content">
                    <h1 style="width: 90%;">Title</h1>
                    <div class="text carousel-description">Lorem Ipsum is simply dummy text of the
                        printing and
                        typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the
                        1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen
                        book. It has survived not only five centuries, but also the leap into electronic typesetting,
                        remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset
                        sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like
                        Aldus PageMaker including versions of Lorem Ipsum.</div>
                </div>
            </div>
        </div>

        @endif

    </div>
</section>

<section class="about-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 about-column">
                <div class="about-content">
                    <div class="title">About BravoLife</div>
                    <div class="text"><?= $aboutFirst->main_slug; ?> </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="cta-section overlay-bg centred"
    style="background-image: url({{ asset('client/images/background/cta-bg.jpg') }});">
    <div class="container">
        <div class="title">BravoLife</div>
        <div class="text">Join your hand with us for a better life and beautiful future</div>
    </div>
</section>

<section class="about-style-two sec-pad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 about-column">
                <div class="about-content">
                    <div class="top-content">
                        <div class="title">Our Services</div>
                        <div class="text"><?=$services->service;?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
