@extends('pre.app.app')

@section('content')

<section class="page-title centred" style="background-image: url({{ $setting->image_url }});">
    <div class="container">
        <div class="content-box">
            <div class="title">Our Services</div>
            <ul class="bread-crumb">
                <li><a href="/">Home</a></li>
                <li>Services</li>
            </ul>
        </div>
    </div>
</section>

<section class="about-style-two sec-pad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 about-column">
                <div class="about-content">
                    <div class="top-content">
                        <div class="text"><?=$service->service;?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- about-style-two -->
@endsection
