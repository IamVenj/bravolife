@extends("pre.app.app")

@section('content')

<!-- page-title -->
<section class="page-title centred" style="background-image: url({{ asset('client/images/bg/7.jpg') }});">
    <div class="container">
        <div class="content-box">
            <div class="title">Event Details</div>
            <ul class="bread-crumb">
                <li><a href="/">Home</a></li>
                <li><a href="/events">Events</a></li>
                <li>Event</li>
            </ul>
        </div>
    </div>
</section>
<!-- page-title end -->


<!-- event-details -->
<section class="event-details sec-pad-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="event-details-content">
                    <div class="content-style-one">
                        <figure class="img-box">
                            <img src="{{ asset('client/images/bg/19.jpg') }}" alt="" height="450px"
                                style="object-fit: cover;">
                            <div class="date"><span>25</span>JUNE</div>
                        </figure>
                        <div class="top-content">
                            <div class="sec-title">Insure Clean Water in South Africa</div>
                            <ul class="info-box">
                                <li><i class="fa fa-clock-o"></i>9:00am-5:00pm</li>
                                <li><i class="fa fa-map-marker"></i>Mirpur, Dhaka</li>
                            </ul>
                        </div>
                        <div class="bold-text">Lorem ipsum dolor sit amet, quo odio atqui tamquam eu, duo ex amet elitr.
                            Ne essent feugiat vim et soluta reprimique instructior mel. Munere tamquam referrentur.
                        </div>
                        <div class="text">
                            <p>Lorem ipsum dolor sit amet, et electram comprehensam sit. Quo an splendide signiferumque,
                                an vix sententiae insctior, laudem corrumpit disputationi sed ei. Eum malis mucius
                                ancillae in, verear nusquam appetere vis id, scaevol atomorum in sed. Eum denique
                                vulputate ne, in eos alienum corrumpit ullamcorper. Vel ea fabulas instructior, agam
                                falli sit an.Ad cum amet graeco consequat, sed ei veri novum appellantur.</p>
                            <p>Qui in quod ubique euismod, consul noster disputationi eos no, nec te latine repudiare.
                                Te pro dolor volutpat. Quo disu omnis appellantur an.Per ea convenire voluptatum, pro
                                copiosae vituperatoribus ut.</p>
                        </div>
                        <div class="title">
                            <h3>Event Colcusion</h3>
                        </div>
                        <ul class="check-list">
                            <li>Lorem ipsum dolor sit amet, usu an quem augue admodum.</li>
                            <li>Lorem ipsum dolor sit amet, duo in atqui omnesque praesent.</li>
                            <li>Lorem ipsum dolor sit amet, iuvaret ancillae id mea sint.</li>
                            <li>Lorem ipsum dolor sit amet, eos corrumpit ullamcorp instruc. tor</li>
                        </ul>
                    </div>
                    <div class="related-event overlay-style-two">
                        <div class="sec-title">Releted Events</div>
                        <div class="related-event-carousel">
                            <div class="single-upcoming-event single-item">
                                <div class="image img-box">
                                    <figure><img src="{{ asset('client/images/bg/6.jpg') }}" alt="" height="250px"
                                            style="object-fit: cover;"></figure>
                                    <div class="overlay">
                                        <div class="overlay-content">
                                            <div class="content">
                                                <a class="link-btn" href="/event">
                                                    <i class="fa fa-link"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="date"><span>30</span>Jan</div>
                                </div>
                                <div class="lower-content">
                                    <h4><a href="/event">Event1</a></h4>
                                    <ul class="info-box">
                                        <li><i class="fa fa-clock-o"></i>9.00 AM - 11.00 PM </li>
                                        <li><i class="fa fa-map-marker"></i>Location</li>
                                    </ul>
                                    <div class="link"><a href="/event" class="theme-btn-two">Read More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="single-upcoming-event single-item">
                                <div class="image img-box">
                                    <figure><img src="{{ asset('client/images/bg/6.jpg') }}" alt="" height="250px"
                                            style="object-fit: cover;"></figure>
                                    <div class="overlay">
                                        <div class="overlay-content">
                                            <div class="content">
                                                <a class="link-btn" href="/event">
                                                    <i class="fa fa-link"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="date"><span>30</span>Jan</div>
                                </div>
                                <div class="lower-content">
                                    <h4><a href="/event">Event1</a></h4>
                                    <ul class="info-box">
                                        <li><i class="fa fa-clock-o"></i>9.00 AM - 11.00 PM </li>
                                        <li><i class="fa fa-map-marker"></i>Location</li>
                                    </ul>
                                    <div class="link"><a href="/event" class="theme-btn-two">Read More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="single-upcoming-event single-item">
                                <div class="image img-box">
                                    <figure><img src="{{ asset('client/images/bg/6.jpg') }}" alt="" height="250px"
                                            style="object-fit: cover;"></figure>
                                    <div class="overlay">
                                        <div class="overlay-content">
                                            <div class="content">
                                                <a class="link-btn" href="/event">
                                                    <i class="fa fa-link"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="date"><span>30</span>Jan</div>
                                </div>
                                <div class="lower-content">
                                    <h4><a href="/event">Event1</a></h4>
                                    <ul class="info-box">
                                        <li><i class="fa fa-clock-o"></i>9.00 AM - 11.00 PM </li>
                                        <li><i class="fa fa-map-marker"></i>Location</li>
                                    </ul>
                                    <div class="link"><a href="/event" class="theme-btn-two">Read More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="single-upcoming-event single-item">
                                <div class="image img-box">
                                    <figure><img src="{{ asset('client/images/bg/6.jpg') }}" alt="" height="250px"
                                            style="object-fit: cover;"></figure>
                                    <div class="overlay">
                                        <div class="overlay-content">
                                            <div class="content">
                                                <a class="link-btn" href="/event">
                                                    <i class="fa fa-link"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="date"><span>30</span>Jan</div>
                                </div>
                                <div class="lower-content">
                                    <h4><a href="/event">Event1</a></h4>
                                    <ul class="info-box">
                                        <li><i class="fa fa-clock-o"></i>9.00 AM - 11.00 PM </li>
                                        <li><i class="fa fa-map-marker"></i>Location</li>
                                    </ul>
                                    <div class="link"><a href="/event" class="theme-btn-two">Read More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="single-upcoming-event single-item">
                                <div class="image img-box">
                                    <figure><img src="{{ asset('client/images/bg/6.jpg') }}" alt="" height="250px"
                                            style="object-fit: cover;"></figure>
                                    <div class="overlay">
                                        <div class="overlay-content">
                                            <div class="content">
                                                <a class="link-btn" href="/event">
                                                    <i class="fa fa-link"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="date"><span>30</span>Jan</div>
                                </div>
                                <div class="lower-content">
                                    <h4><a href="/event">Event1</a></h4>
                                    <ul class="info-box">
                                        <li><i class="fa fa-clock-o"></i>9.00 AM - 11.00 PM </li>
                                        <li><i class="fa fa-map-marker"></i>Location</li>
                                    </ul>
                                    <div class="link"><a href="/event" class="theme-btn-two">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- event-details end -->

@endsection
