@extends('pre.app.app')

@section('content')
<!-- page-title -->
<section class="page-title centred">
    <div class="container">
        <div class="content-box">
            <div class="title">About Us</div>
            <ul class="bread-crumb">
                <li><a href="/">Home</a></li>
                <li>About</li>
            </ul>
        </div>
    </div>
</section>
<!-- page-title end -->


<!-- about-style-two -->
<section class="about-style-two sec-pad">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 about-column">
                <div class="about-content">
                    <div class="top-content">
                        <div class="text"><?=$about->main_slug;?></div>
                    </div>
                    <div class="lower-content">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 column mt-4">
                                <div class="single-item">
                                    <div class="number">01</div>
                                    <h4><a href="#">Our Vision</a></h4>
                                    <div class="text"><?= $about->our_vision;?> </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 column mt-4">
                                <div class="single-item">
                                    <div class="number">02</div>
                                    <h4><a href="#">Our Mission</a></h4>
                                    <div class="text"><?= $about->our_mission;?> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 img-column">
                <figure class="img-box"><img src="{{ $about->image_url }}" alt="" height="800px" width="500px"
                        style="object-fit: cover;"></figure>
            </div>
        </div>
    </div>
</section>
<!-- about-style-two -->

@endsection
