@extends('pre.app.app')

@section('content')
<!-- page-title -->
<section class="page-title centred" style="background-image: url({{ $setting->image_url }});">
    <div class="container">
        <div class="content-box">
            <div class="title">Events</div>
            <ul class="bread-crumb">
                <li><a href="/">Home</a></li>
                <li>Events</li>
            </ul>
        </div>
    </div>
</section>
<!-- page-title end -->
<!-- event-grid -->
<section class="event-grid overlay-style-two sec-pad-2">
    <div class="container">
        <div class="row">
            @if(count($events) > 0)
            @foreach ($events as $event)
            <div class="col-lg-4 col-md-6 col-sm-12 column">
                <div class="single-upcoming-event single-item wow fadeInUp" data-wow-delay="0ms"
                    data-wow-duration="1500ms">
                    <div class="image img-box">
                        <figure><img src="{{ $event->image_url }}" alt="" height="250px" style="object-fit: cover;">
                        </figure>
                        <div class="overlay">
                            <div class="overlay-content">
                                <div class="content">
                                    <a class="link-btn" href="/event">
                                        <i class="fa fa-link"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="date">
                            <span>{{ date('d', $event->date) }}</span>{{ date('m, Y', $event->date) }}
                        </div>
                    </div>
                    <div class="lower-content">
                        <h4><a href="/event">{{ $event->title }}</a></h4>
                        <ul class="info-box">
                            <li><i class="fa fa-clock-o"></i>{{ $event->start_time }} - {{ $event->end_time }} </li>
                            <li><i class="fa fa-map-marker"></i>{{ $event->location }}</li>
                        </ul>
                        <div class="link"><a href="/event" class="theme-btn-two">Read More</a></div>
                    </div>
                </div>
            </div>
            @endforeach
            @else
            <div class="col-md-12">
                <div class="card" style="box-shadow: 0px 10px 10px rgba(0,0,0,0.02); margin-top: 50px;">
                    <p style="text-align: center; font-weight: 100; margin-top: 10px; padding: 45px; font-size: 32px;">
                        There are no events at this time
                    </p>
                </div>
            </div>
            @endif
        </div>
        {{ $events->render() }}
    </div>
</section>
<!-- event-grid end -->

@endsection
