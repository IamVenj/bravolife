@extends('pre.app.app')

@section('content')

<section class="page-title centred" style="background-image: url({{ $setting->image_url }});">
    <div class="container">
        <div class="content-box">
            <div class="title">Accommodation</div>
            <ul class="bread-crumb">
                <li><a href="/">Home</a></li>
                <li>Accommodation</li>
            </ul>
        </div>
    </div>
</section>

<client-accommodation-component></client-accommodation-component>

@endsection
