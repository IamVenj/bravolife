<nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header d-flex align-items-center">
            <a class="navbar-brand" href="dashboard.html">
                <img src="{{ asset('client/images/bravolife.png') }}" class="navbar-brand-img" alt="BravoLife Admin">
            </a>
            <div class="ml-auto">
                <!-- Sidenav toggler -->
                <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
                    <div class="sidenav-toggler-inner">
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <!-- Nav items -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="/dashboard">
                            <i class="ni ni-shop text-primary"></i>
                            <span class="nav-link-text">Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/carousel">
                            <i class="ni ni-chart-pie-35 text-info"></i>
                            <span class="nav-link-text">Carousel</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/about">
                            <i class="ni ni-bulb-61 text-green"></i>
                            <span class="nav-link-text">About</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/accommodation/admin">
                            <i class="ni ni-bulb-61 text-red"></i>
                            <span class="nav-link-text">Accommodation</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/gallery/admin">
                            <i class="ni ni-image text-green"></i>
                            <span class="nav-link-text">Gallery</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/help-and-support/admin">
                            <i class="ni ni-badge text-green"></i>
                            <span class="nav-link-text">Help & Support</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/newsletter">
                            <i class="ni ni-bag-17 text-danger"></i>
                            <span class="nav-link-text">NewsLetter</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/partner">
                            <i class="ni ni-bullet-list-67 text-info"></i>
                            <span class="nav-link-text">Partner</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/referral/admin">
                            <i class="ni ni-paper-diploma text-green"></i>
                            <span class="nav-link-text">Referrals</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/services">
                            <i class="ni ni-box-2 text-primary"></i>
                            <span class="nav-link-text">Service</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/testimony">
                            <i class="ni ni-chat-round text-danger"></i>
                            <span class="nav-link-text">Testimony</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/users/newsletter">
                            <i class="ni ni-user-run text-info"></i>
                            <span class="nav-link-text">Registered Users</span>
                        </a>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link" href="/team">
                            <i class="ni ni-badge text-green"></i>
                            <span class="nav-link-text">Our Team</span>
                        </a>
                    </li> --}}
                    {{-- <li class="nav-item">
                        <a class="nav-link" href="/event/admin">
                            <i class="ni ni-calendar-grid-58 text-red"></i>
                            <span class="nav-link-text">Events</span>
                        </a>
                    </li> --}}
                </ul>
                <!-- Divider -->
                <hr class="my-3">
            </div>
        </div>
    </div>
</nav>
