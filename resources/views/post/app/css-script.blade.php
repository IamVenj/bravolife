<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description"
    content="BravoLife endeavours to help asylum seeking children and Young people, age 16 years to 25 years old, deal with any worries they may have about in care and leaving care by providing them with information on practical help they will need with accomodation, education, training and employment">
<meta name="author" content="Creative Tim">
<title>BravoLife Admin</title>

<!--  Social tags      -->
<meta name="keywords"
    content="BravoLife endeavours to help asylum seeking children and Young people, age 16 years to 25 years old, deal with any worries they may have about in care and leaving care by providing them with information on practical help they will need with accomodation, education, training and ">
<meta name="description"
    content="BravoLife endeavours to help asylum seeking children and Young people, age 16 years to 25 years old, deal with any worries they may have about in care and leaving care by providing them with information on practical help they will need with accomodation, education, training and ">

<!-- Favicon -->
<link rel="icon" href="{{ asset('admin/img/brand/favicon.png') }}" type="image/png">

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700&display=swap">

<link rel="stylesheet" href="{{ asset('admin/vendor/nucleo/css/nucleo.css') }}" type="text/css">
<link rel="stylesheet" href="{{ asset('admin/vendor/%40fortawesome/fontawesome-free/css/all.min.css') }}"
    type="text/css">

<link rel="stylesheet" href="{{ asset('admin/css/argon.min9f1e.css?v=1.1.0') }}" type="text/css">
