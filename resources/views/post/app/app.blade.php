<!DOCTYPE html>
<html>

<head>
    @include('post.app.css-script')
</head>

<body>
    <div class="total-lds">
        <div class="lds-grid">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    @include('post.app.sidebar')
    <div class="main-content" id="panel">
        @include('post.app.nav')
        <div id="app">
            @yield('content')
        </div>
    </div>
    @include('post.app.js-script')
</body>

</html>
