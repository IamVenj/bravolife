<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('admin/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('admin/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('admin/vendor/js-cookie/js.cookie.js') }}"></script>
<script src="{{ asset('admin/vendor/jquery.scrollbar/jquery.scrollbar.min.js') }}"></script>
<script src="{{ asset('admin/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js') }}"></script>

<script src="{{ asset('admin/vendor/chart.js/dist/Chart.min.js') }}"></script>
<script src="{{ asset('admin/vendor/chart.js/dist/Chart.extension.js') }}"></script>
<script src="{{ asset('admin/vendor/jvectormap-next/jquery-jvectormap.min.js') }}"></script>
<script src="{{ asset('admin/js/vendor/jvectormap/jquery-jvectormap-world-mill.js') }}"></script>

<script src="{{ asset('admin/js/argon.min9f1e.js?v=1.1.0') }}"></script>
<script src="{{ asset('admin/js/demo.min.js') }}"></script>
