<!DOCTYPE html>
<html>

<head>
    @include('post.app.css-script')
</head>

<body class="bg-default">
    <div class="total-lds">
        <div class="lds-grid">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <div id="app">
        @yield('content')
    </div>
    @include('post.app.js-script')
</body>

</html>
