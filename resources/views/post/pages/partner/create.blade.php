@extends('post.app.app')
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">Add Partner</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="/"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="{{ route('partner.index') }}"><i
                                        class="fas fa-eye"></i> Partner</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Add Partner</li>
                        </ol>
                    </nav>
                </div>
                <div class="col-lg-6 col-5 text-right">
                    <a href="{{ route('partner.index') }}" class="btn btn-sm btn-neutral"><i class="fas fa-eye"></i>
                        View Partner</a>
                </div>
            </div>
        </div>
    </div>
</div>
<admin-create-partner-component></admin-create-partner-component>
@endsection
