@extends('post.app.app')
@section('content')
<div class="header pb-6 d-flex align-items-center"
    style="min-height: 350px; background-image: url({{ asset('client/images/bg/5.jpg') }}); background-size: cover; background-position: center top;">
    <span class="mask bg-gradient-default opacity-8"></span>
    <div class="container-fluid d-flex align-items-center">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <h1 class="display-2 text-white">Hello {{ \Auth::user()->name }}</h1>
                <p class="text-white mt-0 mb-5">This is your profile page.</p>
            </div>
        </div>
    </div>
</div>
<profile-component></profile-component>
@endsection
