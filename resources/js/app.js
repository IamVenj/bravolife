/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");
import swal from 'sweetalert'
window.Vue = require("vue");
import vue2Dropzone from 'vue2-dropzone'
import 'vue2-dropzone/dist/vue2Dropzone.min.css'
import wysiwyg from "vue-wysiwyg";
Vue.use(wysiwyg, { hideModules: { "image": true } });
import "vue-wysiwyg/dist/vueWysiwyg.css";
Vue.component('pagination', require('laravel-vue-pagination'));
import DateRangePicker from 'vue2-daterange-picker'
import 'vue2-daterange-picker/dist/vue2-daterange-picker.css'
Vue.component('date-range-picker', DateRangePicker)
import "viewerjs/dist/viewer.css";
import Viewer from "v-viewer";
Vue.use(Viewer);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

/**
 * Integration components | Outside Components
 */
Vue.component('vue2Dropzone', vue2Dropzone);

/**
 * pre-login
 * ---------------
 */

Vue.component('referral-component', require('./components/pre/app/ReferralComponent.vue').default);
Vue.component('subscription-component', require('./components/pre/app/SubscriptionComponent.vue').default);
Vue.component('footer-bottom-component', require('./components/pre/app/FooterBottomComponent.vue').default);
Vue.component('contact-form-component', require('./components/pre/app/ContactComponent.vue').default);

/**
 * post-login
 * -------------------
 */

Vue.component('admin-carousel-component', require('./components/post/carousel/CarouselComponent.vue').default);
Vue.component('admin-create-carousel-component', require('./components/post/carousel/CreateCarouselComponent.vue').default);
Vue.component('login-component', require('./components/post/LoginComponent.vue').default);

Vue.component('settings-component', require('./components/post/settings/SettingsComponent.vue').default);
Vue.component('profile-component', require('./components/post/profile/ProfileComponent.vue').default);
Vue.component('about-component', require('./components/post/about/AboutComponent.vue').default);
Vue.component('newsletter-component', require('./components/post/newsletter/NewsLetterComponent.vue').default);
Vue.component('service-component', require('./components/post/service/ServiceComponent.vue').default);
Vue.component('add-service-component', require('./components/post/service/AddServiceComponent.vue').default);
Vue.component('users-component', require('./components/post/newsletter/UsersComponent.vue').default);
Vue.component('admin-partner-component', require('./components/post/partner/PartnerComponent.vue').default);
Vue.component('admin-create-partner-component', require('./components/post/partner/CreatePartnerComponent.vue').default);
Vue.component('admin-testimony-component', require('./components/post/testimony/TestimonyComponent.vue').default);
Vue.component('admin-create-testimony-component', require('./components/post/testimony/CreateTestimonyComponent.vue').default);
Vue.component('admin-team-component', require('./components/post/team/TeamComponent.vue').default);
Vue.component('admin-create-team-component', require('./components/post/team/CreateTeamComponent.vue').default);
Vue.component('admin-event-component', require('./components/post/event/EventComponent.vue').default);
Vue.component('admin-create-event-component', require('./components/post/event/CreateEventComponent.vue').default);
Vue.component('admin-referral-component', require('./components/post/referral/ReferralComponent.vue').default);
Vue.component('admin-archive-referral-component', require('./components/post/referral/ArchiveReferralComponent.vue').default);

Vue.component('admin-help-component', require('./components/post/help/HelpComponent.vue').default);
Vue.component('admin-create-gallery-component', require('./components/post/gallery/CreateGalleryComponent.vue').default);
Vue.component('admin-gallery-component', require('./components/post/gallery/GalleryComponent.vue').default);
Vue.component('admin-image-accommodation-component', require('./components/post/accomodation/AccomodationImageComponent.vue').default);
Vue.component('admin-create-accommodation-component', require('./components/post/accomodation/AddAcommodationComponent.vue').default);
Vue.component('admin-accommodation-component', require('./components/post/accomodation/AccomodationComponent.vue').default);

Vue.component('client-gallery-component', require('./components/pre/ClientGalleryComponent.vue').default);
Vue.component('client-accommodation-component', require('./components/pre/ClientAccommodationComponent.vue').default);

/**
 * Modals
 */
Vue.component('delete-component', require('./components/post/modals/deleteModal.vue').default);
Vue.component('image-update-component', require('./components/post/modals/imageModal.vue').default);
Vue.component('carousel-edit-component', require('./components/post/modals/carousel/editModal.vue').default);
Vue.component('service-edit-component', require('./components/post/modals/service/editModal.vue').default);
Vue.component('newsletter-edit-component', require('./components/post/modals/newsletter/editModal.vue').default);
Vue.component('send-component', require('./components/post/modals/newsletter/sendModal.vue').default);
Vue.component('profile-edit-component', require('./components/post/modals/profile/profileModal.vue').default);
Vue.component('testimony-edit-component', require('./components/post/modals/testimony/editModal.vue').default);
Vue.component('team-edit-component', require('./components/post/modals/team/editModal.vue').default);
Vue.component('event-edit-component', require('./components/post/modals/event/editModal.vue').default);
Vue.component('accommodation-edit-component', require('./components/post/modals/accommodation/editModal.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: "#app"
});
