class Errors {

    constructor() {
        this.errors = {};
    }

    has(key) {
        return this.errors[key] !== undefined
    }

    first(key) {
        if (this.has(key)) {
            return this.errors[key][0]
        }
    }

    get(key) {
        if (this.has(key)) {
            return this.errors[key]
        }
    }

    all() {
        return this.errors
    }

    push(values) {
        Object.assign(this.errors, values);
    }

    fill(values) {
        this.errors = values
    }

    clearErr(key) {
        if (this.has(key)) {
            delete this.errors[key];
        }
    }

    disableBtn(disable) {
        if (Object.keys(this.errors).length > 0) {
            return disable = true;
        } else if (disable === true) {
            return disable = true;
        }
    }

    flush(key) {
        if (this.has(key)) {
            this.errors[key] = {};
        }
    }

}

export default new Errors()
