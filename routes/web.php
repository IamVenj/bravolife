<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/migrate', function () {
    Artisan::call('migrate:fresh --seed');
    return 'successfully migrated!';
});
Route::get('/cache', function () {
    Artisan::call('config:cache');
    return 'successfully cached!';
});

Route::get('/', "HomeController@index");
Route::get('/about', "AboutController@index");
// Route::get('/events', "EventController@index");
// Route::get('/event', "EventController@show");
Route::get('/contact', "ContactController@index");

Route::get('/gallery', "GalleryController@index");
Route::get('/all-gallery', "GalleryController@getGallery");
Route::get('/home-gallery', "GalleryController@getHomeGallery");

Route::get('/accommodation', "AccommodationController@index");
Route::get('/all-accommodation', "AccommodationController@getAccommodation");

Route::get('/help-and-support', "HelpController@index");
Route::get('/service', "ServiceController@index");

Route::post('/subscribe/newsletter', 'NewsletterController@subscribe');
Route::post('/contact/form', 'ContactController@store');
Route::post('/referral/client', 'ReferralController@store');
/**
 * -----------------------------------------------------------------------------
 */
Route::get('/dashboard', 'DashboardController@index');
Route::get('/admin-login', 'AuthController@index')->name("login");
Route::post('/login', 'AuthController@store');
Route::post('/logout', 'AuthController@destroy')->name("logout");
Route::get('/forgot-password', 'Auth\ForgotPasswordController@index');

Route::get('/carousel', "CarouselController@index")->name('carousel.index');
Route::get('/add-carousel', "CarouselController@create")->name('carousel.create');
Route::post('/add-carousel', "CarouselController@store");
Route::patch('/carousel/edit/{id}', "CarouselController@update");
Route::patch('/carousel/edit/image/{id}', "CarouselController@updateImage");
Route::delete('/carousel/destroy/{id}', "CarouselController@destroy");
Route::get('/all-carousel', "CarouselController@getPaginatedCarousel");

Route::get('/settings', "SettingsController@index");
Route::patch('/settings/{id}', "SettingsController@update");
Route::get('/settings/get', "SettingsController@getSettings");

Route::get('/profile', "AccountController@index");
Route::get('/account/auth', "AccountController@getAuthUser");
Route::patch('/account/update/password', "AccountController@updatePassword");
Route::patch('/account/update/user', "AccountController@update");

Route::get('/admin/about', "AboutController@adminIndex")->name('about.admin.index');
Route::get('/admin/about/data', "AboutController@getAboutData");
Route::patch('/admin/about/store/{id}', "AboutController@update");

Route::get('/newsletter', "NewsLetterController@index");
Route::post('/newsletter/search', "NewsLetterController@search");
Route::get('/newsletter/vue', "NewsLetterController@vueIndex");
Route::post('/newsletter/store', "NewsLetterController@store");
Route::patch('/newsletter/update/{id}', "NewsLetterController@update");
Route::post('/newsletter/send/{id}', "NewsLetterController@send");
Route::delete('/newsletter/destroy/{id}', "NewsLetterController@destroy");


Route::get('/admin/services', 'ServiceController@adminIndex')->name('service.index');
Route::get('/admin/services/get', 'ServiceController@getService');
// Route::get('/services/add', 'ServiceController@create')->name('service.create');
Route::patch('/services/store', "ServiceController@store");
// Route::patch('/services/edit/image/{id}', "ServiceController@updateImage");
// Route::get('/all-service', 'ServiceController@getPaginatedService');
// Route::patch('/services/update/{id}', "ServiceController@update");
// Route::delete('/services/destroy/{id}', "ServiceController@destroy");

Route::get('/users/newsletter', 'NewsLetterController@registeredIndex');
Route::post('/users/newsletter/search', 'NewsLetterController@searchUsers');
Route::post('/users/newsletter/destroy/{id}', 'NewsLetterController@searchUsers');
Route::get('/users/newsletter/vue', 'NewsLetterController@getUser');

Route::get('/partner', "PartnerController@index")->name('partner.index');
Route::get('/add-partner', "PartnerController@create")->name('partner.create');
Route::post('/add-partner', "PartnerController@store");
Route::patch('/partner/edit/image/{id}', "PartnerController@update");
Route::delete('/partner/destroy/{id}', "PartnerController@destroy");
Route::get('/all-partner', "PartnerController@getPartners");

Route::get('/testimony', "TestimonyController@index")->name('testimony.index');
Route::get('/add-testimony', "TestimonyController@create")->name('testimony.create');
Route::post('/add-testimony', "TestimonyController@store");
Route::patch('/testimony/edit/{id}', "TestimonyController@update");
Route::patch('/testimony/edit/image/{id}', "TestimonyController@updateImage");
Route::delete('/testimony/destroy/{id}', "TestimonyController@destroy");
Route::get('/all-testimony', "TestimonyController@getTestimony");

// Route::get('/team', "TeamController@index")->name('team.index');
// Route::get('/add-team', "TeamController@create")->name('team.create');
// Route::post('/add-team', "TeamController@store");
// Route::patch('/team/edit/{id}', "TeamController@update");
// Route::patch('/team/edit/image/{id}', "TeamController@updateImage");
// Route::delete('/team/destroy/{id}', "TeamController@destroy");
// Route::get('/all-team', "TeamController@getTeamMembers");

// Route::get('/event/admin', "EventController@adminIndex")->name('event.index');
// Route::get('/add-event', "EventController@create")->name('event.create');
// Route::post('/add-event', "EventController@store");
// Route::patch('/event/edit/{id}', "EventController@update");
// Route::patch('/event/edit/image/{id}', "EventController@updateImage");
// Route::delete('/event/destroy/{id}', "EventController@destroy");
// Route::get('/all-event', "EventController@getEvents");

Route::get('/referral/unArchived/get/vue', 'ReferralController@getUnArchivedReferral');
Route::get('/referral/archived/get/vue', 'ReferralController@getArchivedReferral');
Route::post('/referral/search', 'ReferralController@searchReferral');
Route::get('/referral/admin', 'ReferralController@index');

Route::get('/gallery/admin', 'GalleryController@adminIndex')->name('gallery.index');
Route::get('/gallery/admin/create', 'GalleryController@create')->name('gallery.create');
Route::post('/add-gallery', 'GalleryController@store');
Route::patch('/gallery/edit/image/{id}', 'GalleryController@update');
Route::delete('/gallery/destroy/{id}', 'GalleryController@destroy');

Route::get('/accommodation/admin', 'AccommodationController@adminIndex')->name('accommodation.index');
Route::get('/accommodation/admin/create', 'AccommodationController@create')->name('accommodation.create');
Route::get('/accommodation/admin/images/{id}', 'AccommodationController@showImage')->name('accommodation.image');
Route::get('/accommodation/admin/image/get/{id}', 'AccommodationController@getImages');
Route::post('/add-accommodation', 'AccommodationController@store');
Route::patch('/accommodation/update/settings', 'AccommodationController@updateAccommodationMain');
Route::patch('/accommodation/edit/{id}', 'AccommodationController@update');
Route::patch('/accommodation/edit/image/{id}', 'AccommodationController@updateImage');
Route::delete('/accommodation/image/destroy/{id}', 'AccommodationController@destroyImage');
Route::delete('/accommodation/destroy/{id}', 'AccommodationController@destroy');

Route::get('/help-and-support/admin', 'HelpController@adminIndex')->name('help.index');
Route::get('/help-and-support/admin/get', 'HelpController@getHelp');
Route::patch('/help/update', 'HelpController@store');